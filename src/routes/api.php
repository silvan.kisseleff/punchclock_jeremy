<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/auth')->group(function () {
    // handle login request
    Route::post('login', 'AuthController@login');
    // register new users
    Route::post('register', 'AuthController@register');
    // handle logout
    Route::post('logout', 'AuthController@logout');
    // refresh token timer
    Route::post('refresh', 'AuthController@refresh');
    // return user data
    Route::post('me', 'AuthController@me');
});

Route::prefix('/real/auth')->group(function() {
    Route::post('login', 'RealAuthController@login');
    Route::post('register', 'RealAuthController@register');
});

Route::middleware('jwt.auth')->group(function () {
    // create, edit, delete usersrs
    Route::resource('/users', 'UserController');

    // list entries from user
    Route::get('/users/{username}/entries', 'EntryController@index');
    // create new entry for an user
    Route::post('/users/{username}/entries', 'EntryController@store');
    // return data from an entry
    Route::get('/users/{username}/entries/{id}', 'EntryController@show');
    // update data from entry
    Route::patch('/users/{username}/entries/{id}', 'EntryController@update');
    // remove an entry
    Route::delete('/users/{username}/entries/{id}', 'EntryController@destroy');
});
