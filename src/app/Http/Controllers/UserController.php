<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(User::all()->jsonSerialize(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $this->validateUser($request);
        $attributes['password'] = Hash::make($attributes['password']);
        $user = new User($attributes); 
        $user->save();
        return (new Response(config('messages.success'), 200));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $attributes = validateUserPassword($request);
        $user->password = Hash::make($attributes['password']); 
        $user->save();    
        return (new Response(config('messages.success'), 200));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return (new Response(config('messages.success'), 200));
    }

    /**
     * Validate user params
     *
     * @return \App\User
     */
    public function validateUser($request) {
        return $request->validate([
            'name' => ['required', 'unique:users', 'max:255', 'min:3'],
            'password' => ['required', 'max:255', 'min:3'],
        ]);
    }

    /**
     * Validate user password
     *
     * @return \App\User
     */
    public function validateUserPassword($request) {
        return $request->validate([
            'password' => ['required', 'max:255', 'min:3'],
        ]);
    }
}
