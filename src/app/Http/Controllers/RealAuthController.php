<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\BannedPassword;
class RealAuthController extends Controller
{
    /**
     * Create a new RealAuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['name', 'password']);
        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth('api')->user());
    }
    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'user' => $this->guard()->user(),
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ])->header('Authorization', $token);
    }
    public function guard() {
        return \Auth::Guard('api');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $attributes = $this->validateUser($request);
        if (!BannedPassword::where('password', '=', $attributes['password'])->exists()) {
            $attributes['password'] = Hash::make($attributes['password']);
            $user = new User($attributes); 
            $user->save();
            return response()->json([
                'user' => $user
            ], 200);
        } else {
            return response()->json([
                'error' => 'Password is not allowed!'
            ], 418);
        }
    }

    /**
     * Validate user params
     *
     * @return \App\User
     */
    public function validateUser($request) {
        return $request->validate([
            'name' => ['required', 'unique:users', 'max:255', 'min:3'],
            'password' => ['required', 'max:255', 'min:3'],
        ]);
    }
}