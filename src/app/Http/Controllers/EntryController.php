<?php

namespace App\Http\Controllers;

use App\Entry;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EntryController extends Controller
{
    /**
     * List all entries from an user
     *
     * @param  string $username
     */
    public function index($username) {
        // select user by username
        $user = DB::select('select * from users where name = ?', [$username]);
        // select entries by user_id
        $entries = DB::select('select * from entries where user_id = ?', [$user[0]->id]);

        return response()->json([
            'entries' => $entries
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($username, Request $request)
    {
        $user = User::whereName($username)->firstOrFail();

        $attributes = $this->validateEntry($request);
        $attributes['user_id'] = $user->id;
        $entry = new Entry($attributes); 

        $entry->save();
        
        return response()->json([
            'entry' => $entry
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function show($username, $entry)
    {
        return response()->json([
            'entry' => Entry::find($entry)
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function update($username, Request $request, $entry)
    {
        $attributes = $this->validateEntry($request);
        $entry = Entry::find($entry);
        $entry->check_in = $attributes['check_in'];
        $entry->check_out = $attributes['check_out'];
        $entry->save();

        return response()->json([
            'entry' => $entry
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function destroy($username, $entry)
    {
        $entry = Entry::find($entry);
        $entry->delete();
        
        return response()->json([
        ], 200);
    }

    /**
     * Validate entry params
     *
     * @return \App\Entry
     */
    public function validateEntry($request) {
        return $request->validate([
            'check_in' => ['required', 'date_format:Y-m-d H:i:s'],
            'check_out' => ['required', 'date_format:Y-m-d H:i:s', 'after:check_in'],
        ]);
    }
}
