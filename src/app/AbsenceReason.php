<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AbsenceReason
 *
 * @property int $id
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbsenceReason newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbsenceReason newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbsenceReason query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbsenceReason whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbsenceReason whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbsenceReason whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbsenceReason whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AbsenceReason extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
    ];
}
