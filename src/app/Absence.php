<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Absence
 *
 * @property int $id
 * @property int $user_id
 * @property int $absence_reason
 * @property string $check_in
 * @property string $check_out
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\AbsenceReason $reason
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absence newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absence newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absence query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absence whereAbsenceReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absence whereCheckIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absence whereCheckOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absence whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absence whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absence whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absence whereUserId($value)
 * @mixin \Eloquent
 */
class Absence extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'check_in', 'check_out', 'user_id', 'absence_reason',
    ];

    /**
     * Get the absence reason object.
     * 
     * @method AbsenceReason
     */
    public function reason() {
        return $this->belongsTo('App\AbsenceReason', 'absence_reason');
    }

    /**
     * Get the user object.
     * 
     * @method User
     */
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
