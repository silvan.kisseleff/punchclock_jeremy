import Login from './components/auth/Login.vue';
import Register from './components/auth/Register.vue';

import EntryMain from './components/entries/Main.vue';
import EntryList from './components/entries/List.vue';
import EditEntry from './components/entries/Edit.vue';
import NewEntry from './components/entries/New.vue';

export const routes = [
    {
        path: '/login',
        component: Login
    },
    {
        path: '/register',
        component: Register
    },
    {
        path: '/entries',
        component: EntryMain,
        meta: {
            requiresAuth: true
        },
        children: [
            {
                path: '/',
                component: EntryList
            },
            {
                path: 'new',
                component: NewEntry
            },
            {
                path: ':id',
                component: EditEntry
            }
        ]
    }
]