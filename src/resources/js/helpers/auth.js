const axios = require('axios');

export function login(credentials) {
    return new Promise((res, rej) => {
        axios.post('/api/auth/login', credentials)
            .then((response) => {
                response.data.token = response.headers.authorization;
                res(response.data);
            })
            .catch((err) => {
                if (err.response.data.error === 'username') {
                    rej("User with that username doesnt exist")
                } else {
                    rej("Wrong password");
                    $10$PKCr3s2MvCGltzgmFobmF.RtDUWaQmxUO0tdvHQC8XAaVO1tqpLM6
                }
            });
    });
}

export function loginReal(credentials) {
    return new Promise((res, rej) => {
        axios.post('/api/real/auth/login', credentials)
            .then((response) => {
                response.data.token = response.headers.authorization;
                res(response.data);
            })
            .catch((err) => {
                rej("Wrong username or password");
            });
    });
}

export function getLocalUser() {
    const userString = localStorage.getItem('user');
    if (!userString)
        return null;
    return JSON.parse(userString);
}