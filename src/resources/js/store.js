import { getLocalUser } from "./helpers/auth";
import axios from "axios";

const user = getLocalUser();

export default {
    state: {
        currentUser: user,
        isLoggedIn: !!user,
        loading: false,
        authError: null,
        entries: []
    },
    getters: {
        isLoading(state) {
            return state.loading;
        },
        isLoggedIn(state) {
            return state.isLoggedIn;
        },
        currentUser(state) {
            return state.currentUser;
        },
        authError(state) {
            return state.authError;
        },
        entries(state) {
            return state.entries;
        }
    },
    mutations: {
        login(state) {
            state.loading = true;
            state.authError = null;
        },
        loginSuccess(state, payload) {
            state.authError = null;
            state.isLoggedIn = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.user, {token: payload.token});

            localStorage.setItem('user', JSON.stringify(state.currentUser));
        },
        loginFailed(state, payload) {
            state.loading = false;
            state.authError = payload.err;
        },
        logout(state) {
            localStorage.removeItem('user');
            state.isLoggedIn = false;
            state.currentUser = null;
        },
        updateEntries(state, payload) {
            state.entries = payload;
        }
    },
    actions: {
        login(context) {
            context.commit('login')
        },
        getEntries(context) {
            axios.get(`/api/users/${context.state.currentUser.name}/entries`, {
                headers: {
                    "Authorization": `Bearer ${context.state.currentUser.token}`
                }
            })
            .then((res) => {
                context.commit('updateEntries', res.data.entries);
            })
        }
    }
}