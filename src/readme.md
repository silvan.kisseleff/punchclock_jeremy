# Punchclock

## Inhalt
1. Beschreibung
2. Installation
3. REST-Schnittstellen
4. Anhang

## Bechreibung
### Ausgangslage
Es soll eine Applikation entwickelt werden, in der man Zeiten erfassen kann. Jeder Zeitraum soll einen Startpunkt und einen Entpunkt haben und zu einem Benutzer gehören. Es sollen Benutzeraccounts erstellt werden können und diese Nutzer sollen die Möglichkeit haben sich anzumelden. Wenn man Einsicht auf Einträge erhalten will, werden nur diejenigen angezeigt, welche man selbst erstellt hat.

### Umsetzung
Die Applikation soll mit einer REST-API als Backend und einem beliebigen Frontend umgesetzt werden. Die Daten sollen persistent in einer Datenbank hinterlegt werden.
Ich werden das Backend der Applikation mit Laravel und das Frontend mit Vue.js umsetzen. Für die Authorisierung wurde die Vorgabe gegeben, diese mittels JWT umzusetzen. 
Als Datenbank werde ich MySQL verwenden.

## Installation
Bevor man das Programm ausführen kann werden noch einige Programme benötigt, welche in der Folgenden Liste aufgelistet sind:

- Php7
- MySQL
- Git
- [Node.js Version 12.13.0](https://nodejs.org/)
- [Composer Version 1.9.1](https://getcomposer.org)

### Installation / Konfiguration
1. Composter Module installieren
  * In der Commandline `composer install` ausführen.
2. Node Konfigurieren
  * In der Commandline `npm install` ausführen.
  * Um die Dateien zu builden muss man nach der Installation noch `npm run dev` ausführen.
3. Datenbank Konfigurieren
  * Hierzu muss zunächst mal die .env.example Datei doubliziert werden und in .env umbenannt werden. 
  * In der Datei müssen die Parameter für die Datenbank angepasst werden, sodass sie mit den Datenbanknutzer übereinstimmen.
  * Man muss noch eine Datenbank in MySQL erstellen, welche den Namen hat, welcher in der Konfigurationsdatei bestimmt wurde.
  * Als letztes muss noch `php artisan migrate` augeführt werden, damit die Tabellen für dei Datenbank erstellt werden.
4. Laravel starten
  * Bevor man die Applikation starten kann muss man noch einen Private key für JWT erstellt werden `php artisan jwt:token`
  * Und noch einen Private Key für Laravel `php artisan key:generate`
  * Um Laravel zu starten muss man in der Commandline `php artisan serve` ausführen.

## REST-Schnittstellen
Bisher wurden nur die Routes für Auth und die Entries erstellt.

| Route | Parameter | Funktion |
| ----- | --------- | -------- |
| POST /api/auth/login | name, password | Liefert bei erfolgreichem Login einen JWT zurück |
| POST /api/auth/register | name, password | Erstellt einen neuen Account |
| GET /api/users/{username} | | Liefert Informationen über den Benutzer zurück |
| GET /api/users/{username}/entries | | Liefert alle Entires des Benutzers zurück |
| GET /api/users/{username}/entries/{entry} | | Liefert Informationen eines Entries zurück |
| POST /api/users/{username}/entries | check_in, check_out | Erstellt eine neues Entry |
| PATCH /api/users/{username}/entries/{entry} | check_in, check_out | Updatet das Entry |
| DELETE /api/users/{username}/entries/{entry} | | Löscht das Entry |
| GET /api/users/{username}/absences | | Leifert alle Absenzen des Benutzers zurück |
| GET /api/users/{username}/absences/{absence} | | Liefert Informationen einer Absenz zurück |
| POST /api/users/{username}/absences | check_in, check_out, reason | Erstellt eine neue Absenz |
| PATCH /api/users/{username}/absences/{absence} | check_in, check_out, reason | Updatet die Absenz |
| DELETE /api/users/{username}/absences/{absence} | | Löscht die Absenz |

## Anhang
Bei der Erstellung von Entries muss darauf geachtet werden, dass das Zeitformat mit dem von MySQL übereinstimmt.

Hier werde ich einige Dateien angeben, in denen sich Codeschnipsel für Aufgaben befiden.

- Die Scripts mit denen ich Bespieldaten für die Datenbank generiere, befinden sich im Ordner /database/factories. Für jede Entität wird eine neue Factory mit dem entsprechenden Namen erstellt.
- Die Models der Entitäten befinden sich im Ordner /app
- Die Controller im Ordner /app/http/controller
- Die Migrations im Ordner /database/migrations
- Ich habe eine Custom SQL-Abfrage im Entry-Controller under der Methode index erstellt
