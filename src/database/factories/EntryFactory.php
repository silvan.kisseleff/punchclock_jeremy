<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Entry::class, function (Faker $faker) {
    $users = App\User::pluck('id')->toArray();
    return [
        'check_in' => $faker->dateTimeThisYear($max = '+1 year')->format('Y-m-d H:i:s'),
        'check_out' => $faker->dateTimeThisYear($max = '+1 year')->format('Y-m-d H:i:s'),
        'user_id' =>  $faker->randomElement($users)
    ];
});
